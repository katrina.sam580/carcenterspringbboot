package com.example.demo.repositery;

import com.example.demo.model.Parameter;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParameterOfCarRepositery extends CrudRepository<Parameter,String> {

    @Query("SELECT valueOfPara FROM Parameter p WHERE p.keyOfPara='numSetts'")
    public String getNumSetDefault();

    @Query("SELECT valueOfPara FROM Parameter p WHERE p.keyOfPara='ProfitRatio'")
    public String getProfitRatioDefault();

    Parameter findByKeyOfPara(String keyOfPara);
}
