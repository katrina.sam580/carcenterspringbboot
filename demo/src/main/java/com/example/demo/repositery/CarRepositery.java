package com.example.demo.repositery;

import com.example.demo.model.Car;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface CarRepositery extends CrudRepository<Car,Integer> {




@Query("SELECT c FROM Car c WHERE c.name_buyer LIKE %?1%"
        + " OR c.name_car LIKE %?1%"
        + " OR CONCAT (c.car_id,'') LIKE %?1%"
        + " OR CONCAT (c.num_sets,'') LIKE %?1%"
        + " OR CONCAT (c.date_sale,'') LIKE %?1%"
        + " OR CONCAT (c.price_car,'') LIKE %?1%"
        + " OR CONCAT (c.price_sale,'') LIKE %?1%"
        + " OR c.is_sale LIKE %?1%"
)
public List<Car> search(String keyword);

//get all info about cars from mssage 
@Query(" SELECT c FROM Car c WHERE c.is_sale=true AND month(c.date_sale) = :month ")
List<Car> getAllInfoOfCarsInMonth(@Param("month") Integer month);


@Query(" SELECT c FROM Car c WHERE c.is_sale=true ")
List<Car> getAllInSellingCars();

// Car findByName_car(String name);
}
