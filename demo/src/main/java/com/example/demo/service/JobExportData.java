package com.example.demo.service;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JobExportData {

    @Autowired
    JobLauncher jobLauncher;

    @Autowired 
    Job job;

  
      //for lunching Job to export data to csv file
      public void lunchJobExport(String month , String email , String content, double finalPrice)  throws Exception
      {
         JobParameters jobParameters = 
         new JobParametersBuilder() 
         .addLong("time",System.currentTimeMillis())   // add time parameter to create new Instance of Job
         .addString("email",email)
         .addString("content",content)
         .addDouble("finalPrice",finalPrice)
         .addString("monthmessage", month).toJobParameters();
          
         JobExecution execution = jobLauncher.run(job, jobParameters);
         System.out.println("Exit Status : " + execution.getStatus());
      }
}
