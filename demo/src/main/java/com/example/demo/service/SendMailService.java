package com.example.demo.service;


import java.io.File;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.core.io.ClassPathResource;
@Service
public class SendMailService {
    

    @Autowired
    private JavaMailSender javaMailSender;


    public void SendMailWithAttach(String Email,String Content,String finalPrice,String filepath) throws MessagingException , IOException
    {
        MimeMessage msg = javaMailSender.createMimeMessage();

        // true = multipart message
        MimeMessageHelper helper = new MimeMessageHelper(msg,true);
        helper.setTo(Email);

        helper.setSubject("Testing from Spring Boot");
        helper.setText("content : "+Content+"\n"+"final price of selling car : "+finalPrice);
        FileSystemResource res = new FileSystemResource(new File(filepath));
        helper.addAttachment("AllSellingCar.csv", res);

        javaMailSender.send(msg);

    }



    public void sendToMessageEmail(String Email,String Content,String finalPrice,String filepath) throws MessagingException
    {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessage = new MimeMessageHelper(message,true);
   
        
        mimeMessage.setTo(Email);
        mimeMessage.setSubject("Mssage From Spring boot Project");
        mimeMessage.setText("content : "+Content+"\n"+"final price of selling car : "+finalPrice);
        FileSystemResource res = new FileSystemResource(new File(filepath));
        mimeMessage.addAttachment("AllSellingCars", res);
        javaMailSender.send(message);

       
    }

    
    
}
