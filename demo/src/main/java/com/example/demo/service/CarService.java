package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Car;
import com.example.demo.repositery.CarRepositery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CarService {

    @Autowired
    CarRepositery carRepositery;
    
    public List<Car> getAllSellingCars(Integer month)
    {
        List<Car> cars = carRepositery.getAllInfoOfCarsInMonth(month);

        return cars;
    }

    public List<Car> getAllCars()
    {
        return carRepositery.getAllInSellingCars();
    }

    public double calculateFinalPriceOfSellingCars(Integer month)
    {
        double finalPrice=0.0;
        List<Car> cars = getAllSellingCars(month);
        for(Car car : cars)
        {
            finalPrice += car.getPrice_sale(); 
        }

        return finalPrice;
    }
    
    public Car addCar(Car car){
        //  return userRepositery.findById(user_id).map(user -> {car.setUser(user);

          return   carRepositery.save(car);
      }
}
