package com.example.demo.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;

import com.example.demo.config.JMSReceiver;
import com.example.demo.config.MessageQueue;
import com.example.demo.model.Car;
import com.example.demo.repositery.CarRepositery;
import com.example.demo.service.CarService;
import com.example.demo.service.SendMailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

@RestController
public class MessageConsumer {
    
    @Autowired
    JMSReceiver receiver;

    @Autowired
    CarService carService;

    @Autowired
    SendMailService sendMailService;


    @GetMapping("/consume")    //just for test
    public MessageQueue  getMessageConsume()
    {
        return receiver.getMessageQueue();
    }

    //get all info about selling cars in the same month from recive message  --just for test--
    @GetMapping("/cars/getallinfo")
    public List<Car> getAllCarsFromMessageInfo()
    {
       //String month = receiver.getMessageQueue().getDateOfCars().substring(3, 5);
       List<Car> cars = carService.getAllSellingCars(Integer.parseInt("12"));
       return cars;
    }

    //send result to email
    @GetMapping("/send/email")
    public String sendresulttoemail()
    {
      MessageQueue message = receiver.getMessageQueue();
      String Email = message.getEmail();
      String Content = message.getContent();
      String month = receiver.getMessageQueue().getDateOfCars().substring(3, 5);

      double finalPrice = carService.calculateFinalPriceOfSellingCars(Integer.parseInt(month));
      try{ 
      sendMailService.sendToMessageEmail(Email, Content, String.valueOf(finalPrice),"C:\\Users\\Fouad\\Downloads");
      }
      catch(MessagingException e){

      }
      return "successfull send email";
    }

}
