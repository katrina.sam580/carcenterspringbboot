package com.example.demo.config;

import org.springframework.stereotype.Component;


import com.example.demo.service.CarService;
import com.example.demo.service.JobExportData;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class JMSReceiver
{
	MessageQueue messageQueue;

	@Autowired
	JobExportData job;

	@Autowired
	CarService carService;

	JMSReceiver(){}

	JMSReceiver(MessageQueue messageQueue){
		this.messageQueue = messageQueue;
	}
	
	@RabbitListener(queues = "message_queue" ,containerFactory = "jsaFactory")
	public void receiveMessage(MessageQueue message)
	{
		
	   System.out.println("Received Message : "+ message);
	   String month = message.getDateOfCars().substring(3, 5);
	   String email = message.getEmail();
	   String content = message.getContent();
	   double finalprice = carService.calculateFinalPriceOfSellingCars(Integer.parseInt(month));
	   
	   this.setMessageQueue(message);

	   try{
		job.lunchJobExport(month,email,content,finalprice);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public MessageQueue getMessageQueue()
	{
		return this.messageQueue;
	}

	public void setMessageQueue(MessageQueue messageQueue)
	{
		this.messageQueue=messageQueue;
	}

}
