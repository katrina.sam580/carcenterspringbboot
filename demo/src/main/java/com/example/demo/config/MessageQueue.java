package com.example.demo.config;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageQueue implements Serializable{


    private String Email;
    private String Content;
    private String DateOfCars;

   
    MessageQueue(@JsonProperty("email") String Email,@JsonProperty("content") String Content,@JsonProperty("dateOfCars") String DateOfCars){
        this.Email=Email;
        this.Content=Content;
        this.DateOfCars=DateOfCars;
    }

    public String getEmail()
    {
        return this.Email;
    }
    public String getContent()
    {
        return this.Content;
    }
    public String getDateOfCars()
    {
        return this.DateOfCars;
    }

    public void SetEmail(String Email)
    {
        this.Email = Email;
    }

    public void SetContent(String Content)
    {
        this.Content = Content;
    }

    
    @Override
    public String toString() {
        return "Message{" +
                "Email='" + this.Email + 
                ", Content=" + this.Content +
                ", Date=" + this.DateOfCars +
                '}';
    }
    
}
