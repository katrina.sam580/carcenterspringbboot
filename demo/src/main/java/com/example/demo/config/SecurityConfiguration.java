package com.example.demo.config;


import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import org.springframework.web.servlet.config.annotation.CorsRegistry;


@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

 


    public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedOrigins("*")
		.allowedMethods("HEAD", "GET", "PUT", "POST",
		"DELETE", "PATCH").allowedHeaders("*");
	}

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        
        http   .cors().and().csrf().disable()
                .authorizeRequests()
                   // Authentication pages do not require permissions
                    .antMatchers(
                           "/authenticate",
                            "/index",
                            "/cars/**",
                            "/message",
                            "/favicon.ico",
                            // "/cars/**",
                            "/sendemail",
                            "/get/allinfocars",
                            "/cars/exportcsv",

                            "/send/email",
                            "/registration**",
                            "/consume",
                            "/js/**",
                            "/css/**",
                            "/img/**",
                            "/webjars/**").permitAll()
                    .anyRequest().authenticated().
                     //Verify that the request is correct
                   // http.addFilterBefore(jwtrequestfilter, UsernamePasswordAuthenticationFilter.class)
                    
                     and()
                
                    .formLogin()
                        .loginPage("/login")
                            .permitAll()
                .and()
                    .logout()
                        .invalidateHttpSession(true)
                        .clearAuthentication(true)
                        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                        .logoutSuccessUrl("/login?logout")
                .permitAll()


                .and()
                //Do not use session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
                 
    }

}
