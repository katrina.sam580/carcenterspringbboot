package com.example.demo.batchprocessing;

import java.io.File;



import javax.sql.DataSource;

import com.example.demo.model.Car;
import com.example.demo.service.CarService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;

import org.springframework.batch.core.Step;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;

import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;


@Configuration
@EnableBatchProcessing
public class BatchConfiguration {
   
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired   
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public DataSource dataSource;

    @Autowired
    CarService carService;
   
    public static String month;
    public static String email;
    public static String content;
    public static double finalprice;

    private final String[] CSV_MAPPING_NAMES = {"car_id","name_car","num_sets","date_sale","price_car","price_sale","name_buyer","is_sale"};

    
    private String SELECT_STMT = "SELECT car_id,name_car,num_sets,date_sale,price_car,price_sale,name_buyer,is_sale FROM car WHERE is_sale=true AND MONTH(date_sale)= ";

    private String SELECT_FINAL_PRICE = "SELECT SUM(price_sale) FROM car WHERE is_sale=true AND MONTH(date_sale)= ";

    Logger log = LoggerFactory.getLogger(BatchConfiguration.class);



    @Bean
    @StepScope
    public JdbcCursorItemReader<Car> reader(@Value("#{jobParameters['monthmessage']}") String monthmessage)
    {
        
       JdbcCursorItemReader<Car> reader = new JdbcCursorItemReader<Car>();
       reader.setName("reader");
       reader.setDataSource(dataSource);
       reader.setSql(SELECT_STMT+ monthmessage);
       
       reader.setRowMapper(new RowMapperCar());
       
       month=monthmessage;
       
       return reader;
    }


     
    
    public BatchItemProcessor processor()
    {
        return   new BatchItemProcessor();
    }

    @Bean
    public FlatFileItemWriter<Car> writer()
    {
        FlatFileItemWriter<Car> writer = new FlatFileItemWriter<Car>();
        FileSystemResource res = new FileSystemResource(new File("carsSelling.csv"));
        writer.setResource(res);
        writer.setLineAggregator(new DelimitedLineAggregator<Car>()
        {{
            setDelimiter(",");
            setFieldExtractor(new BeanWrapperFieldExtractor<Car>()
            {{
                setNames(CSV_MAPPING_NAMES);

            }});
        }});

        
        return writer;
    }

    @Bean    //using chunk
    public Step step1()
    {
        return stepBuilderFactory.get("step1").<Car,Car> chunk(10)
        .reader(reader(month))  //read data from db
        .processor(processor())
        .writer(writer())      //write data to file
        .build();
    }

  

    @Bean
    public Step step2()
    {
        return stepBuilderFactory.get("step2")
               .tasklet(sendToEmailTaskLet(email,content,finalprice))
               .build();
    }

   
    @Bean
    @StepScope
    public SendToEmailTaskLet sendToEmailTaskLet(@Value("#{jobParameters['email']}") String emailmessage ,@Value("#{jobParameters['content']}") String contentmessage,@Value("#{jobParameters['finalPrice']}") double finalpricem)
    {
        SendToEmailTaskLet tasklet = new SendToEmailTaskLet();
       
        tasklet.SetEmail(emailmessage);
        tasklet.SetContent(contentmessage);
        tasklet.SetFinalPrice(finalpricem);

        email = emailmessage;
        content = contentmessage;
        finalprice = finalpricem;
        return tasklet;
    }

 

    @Bean
    public Job exportDataCars() 
    {
        return jobBuilderFactory.get("exportDataCars")
        .incrementer(new RunIdIncrementer())
        .flow(step1())    //step 1 to export data 
        .next(step2())  // step 2 to send data and info to email
        .end()
        .build();
    }

  
}
