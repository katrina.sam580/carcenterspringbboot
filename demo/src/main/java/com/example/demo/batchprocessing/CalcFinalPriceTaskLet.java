package com.example.demo.batchprocessing;

import java.io.IOException;

import javax.mail.MessagingException;

import com.example.demo.service.CarService;
import com.example.demo.service.SendMailService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;


public class CalcFinalPriceTaskLet implements Tasklet, InitializingBean {


    @Autowired
    CarService carService;

    private String month;

    private double finalPrice;
    Logger log = LoggerFactory.getLogger(CalcFinalPriceTaskLet.class);
  public RepeatStatus execute(StepContribution contribution , ChunkContext chunkContext) throws Exception {

    //todo calc final price 
    
    double fp = carService.calculateFinalPriceOfSellingCars(Integer.parseInt(month));
    this.SetFinalPrice(fp);
    log.info("------------------------ : "+fp);
    return RepeatStatus.FINISHED;
  }

  public void afterPropertiesSet() throws Exception {

  }

 

  public void SetMonth(String month)
  {
      this.month=month;
  }
  public void SetFinalPrice(double finalPrice)
  {
      this.finalPrice=finalPrice;
  }
 
   public double getFinalPrice()
   {
       return this.finalPrice;
   } 
}
