package com.example.demo.batchprocessing;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.demo.model.Car;

import org.springframework.jdbc.core.RowMapper;

public class RowMapperCar implements RowMapper<Car> {
    @Override
    public Car mapRow(ResultSet rs , int rowNum) throws SQLException
    {
       
       Car car = new Car();
       car.setCar_id(rs.getInt("car_id"));
       car.setName_car(rs.getString("name_car"));
       car.setNum_sets(rs.getInt("num_sets"));
       car.setDate_sale(rs.getDate("date_sale"));
       car.setPrice_car(rs.getDouble("price_car"));
       car.setPrice_sale(rs.getDouble("price_sale"));
       car.setName_buyer(rs.getString("name_buyer"));
       car.setIs_sale(rs.getBoolean("is_sale"));
       
       return car;
    }
}

