package com.example.demo.batchprocessing;

import java.io.IOException;

import javax.mail.MessagingException;

import com.example.demo.service.SendMailService;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;


public class SendToEmailTaskLet implements Tasklet, InitializingBean {


    @Autowired
    SendMailService sendMailService;


    private String Email;
    private String Content;
    private double finalPrice;
    private String filepath;

  public RepeatStatus execute(StepContribution contribution , ChunkContext chunkContext) throws Exception {

    //todo send to email

      try{ 
        sendMailService.SendMailWithAttach(Email, Content,String.valueOf(finalPrice),"C:\\Users\\katren\\Desktop\\SpringFinalProject\\demo\\carsSelling.csv");
        }
        catch(MessagingException e){
  
        }
        catch(IOException ex){}

    return RepeatStatus.FINISHED;
  }

  public void afterPropertiesSet() throws Exception {

  }

  public void SetEmail(String email)
  {
      this.Email=email;
  }
  public void SetContent(String content)
  {
      this.Content=content;
  }
  public void SetFinalPrice(double finalPrice)
  {
      this.finalPrice=finalPrice;
  }
  public void SetFilePath(String filepath)
  {
      this.filepath=filepath;
  }
    
}
