package com.example.demo.model;



import javax.persistence.*;


@Entity
public class Parameter {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long parameter_id;


    private String keyOfPara;

    private String valueOfPara;

    Parameter(String keyOfPara, String valueOfPara)
    {
        this.keyOfPara=keyOfPara;
        this.valueOfPara=valueOfPara;
    }
    Parameter(){

    }

    public void setParameter_id(long parameter_id) {
        this.parameter_id = parameter_id;
    }

    public void setKeyOfPara(String keyOfPara) {
        this.keyOfPara = keyOfPara;
    }

    public void setValueOfPara(String valueOfPara) {
        this.valueOfPara = valueOfPara;
    }

    public long getParameter_id() {
        return parameter_id;
    }

    public String getKeyOfPara() {
        return keyOfPara;
    }

    public String getValueOfPara() {
        return valueOfPara;
    }
}
