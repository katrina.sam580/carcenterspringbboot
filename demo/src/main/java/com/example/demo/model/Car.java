package com.example.demo.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;

import java.io.Serializable;
import java.sql.Date;

@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer car_id;

    @Column(name = "version")
    @JsonProperty
//    @Version
    private Integer version;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "num_sets")
    @JsonProperty
    private Integer num_sets;

    @Column(name = "date_sale")
    @JsonProperty
    private Date date_sale;

    @Column(name = "name_car")
    @JsonProperty
    private String name_car;

    @Column(name = "is_sale")
    @JsonProperty
    private boolean is_sale ;

    @Column(name = "price_car")
    @JsonProperty
    private Double price_car;

    @Column(name = "price_sale")
    @JsonProperty
    private Double price_sale;

    @Column(name = "name_buyer")
    @JsonProperty
    private String name_buyer;



   public Car(){}


   public Car(Integer car_id,Integer numSets,String namecar,Date dateSale,boolean isSale,Double PriceOfCar,Double PriceOfSale,String nameOfBuyer)
    {
        this.car_id=car_id;
        this.num_sets=numSets;
        this.date_sale=dateSale;
        this.price_car=PriceOfCar;
        this.price_sale=PriceOfSale;
        this.name_buyer=nameOfBuyer;
        this.is_sale=isSale;
        this.name_car=namecar;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getVersion() {
        return version;
    }

    

    public boolean getIs_sale() {
        return is_sale;
    }

    public void setId(long id) {
        this.car_id = Math.toIntExact(id);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setCar_id(Integer car_id) {
        this.car_id = car_id;
    }

    public void setDate_sale(Date dateSale) {
        this.date_sale = dateSale;
    }

    public void setName_buyer(String nameOfBuyer) {
        this.name_buyer = nameOfBuyer;
    }

    public void setNum_sets(Integer numSets) {
        this.num_sets = numSets;
    }

    public void setPrice_car(Double priceOfCar) {
        this.price_car = priceOfCar;
    }

    public void setPrice_sale(Double priceOfSale) {
        this.price_sale = priceOfSale;
    }

    public void setIs_sale(boolean sale) {
        this.is_sale = sale;
    }

    public Integer getNum_sets() {
        return this.num_sets;
    }

    public Date getDate_sale() {
        return this.date_sale;
    }
   
    public Double getPrice_car() {
        return this.price_car;
    }

    public Double getPrice_sale() {
        return this.price_sale;
    }

    public Integer getCar_id() {
        return this.car_id;
    }

    public String getName_buyer() {
        return this.name_buyer;
    }

    public User getUser() {
        return user;
    }

    public void setName_car(String name_car) {
        this.name_car = name_car;
    }

    public String getName_car() {
        return this.name_car;
    }
}
