package com.example.demo;

import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.example.demo.batchprocessing.BatchConfiguration;
import com.example.demo.model.Car;
import com.example.demo.repositery.CarRepositery;
import com.example.demo.service.CarService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


import org.hamcrest.core.Is;
import org.junit.After;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.test.JobLauncherTestUtils;
import org.springframework.batch.test.JobRepositoryTestUtils;
import org.springframework.batch.test.context.SpringBatchTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.core.annotation.Order;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;

import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;


//testing the Spring boot job bath
// @TestExecutionListeners({ DependencyInjectionTestExecutionListener.class})
// @DirtiesContext(classMode = ClassMode.AFTER_CLASS)
@RunWith(SpringRunner.class)
@SpringBatchTest
@EnableAutoConfiguration
@ContextConfiguration(classes = {BatchConfiguration.class,CarService.class})
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
 @Transactional(propagation = Propagation.NOT_SUPPORTED)
public class ExportDataJobTesting {


    @Autowired
    private JobLauncherTestUtils jobLauncherTestUtils;


    @Autowired
    private JobRepositoryTestUtils jobRepositoryTestUtils;

    @Autowired
    CarService carService;

    @Resource
    CarRepositery carRepo;

    @Autowired
    TestEntityManager entityManager;

    
   

    @org.junit.Before
    @Rollback(value = false)

    public void setUp()
    {
        ObjectMapper objMapping = new ObjectMapper();

       TypeReference<List<Car>> typeReference = new TypeReference<List<Car>>(){};
       
       InputStream srcInput = TypeReference.class.getClassLoader().getResourceAsStream("Json/car.json");
       try{
       List<Car> cars = objMapping.readValue(srcInput, typeReference);
        

        entityManager.persist(cars);
    

       }
       catch(IOException e)
       {
           e.printStackTrace();
       }
    }

    //Testing Step1 in job

    @Test
    @Rollback(false)
    @Order(1)
    public void testStep1ExportData()  throws Exception
    {
        
      

            JobExecution jobExecution = jobLauncherTestUtils.launchStep(
            "step1", defaultJobParameters()); 
            Collection actualStepExecutions = jobExecution.getStepExecutions();
            ExitStatus actualJobExitStatus = jobExecution.getExitStatus();


            assertThat(actualJobExitStatus.getExitCode(), Is.is("COMPLETED"));
            System.out.println("finished tes ....");

    }
    

    @After
    public void cleanUp() {
        jobRepositoryTestUtils.removeJobExecutions();
    }

    private JobParameters defaultJobParameters() {
        JobParametersBuilder paramsBuilder = new JobParametersBuilder();
        paramsBuilder
        .addLong("time",System.currentTimeMillis())   // add time parameter to create new Job
        .addString("monthmessage", "12").toJobParameters();
        return paramsBuilder.toJobParameters();
   }
}
