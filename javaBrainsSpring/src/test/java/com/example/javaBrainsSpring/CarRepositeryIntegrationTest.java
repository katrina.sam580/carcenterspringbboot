package com.example.javaBrainsSpring;

import com.example.javaBrainsSpring.model.Car;
import com.example.javaBrainsSpring.model.Parameter;
import com.example.javaBrainsSpring.repository.CarRepositery;
import com.example.javaBrainsSpring.repository.ParameterOfCarRepositery;
import com.example.javaBrainsSpring.service.ParameterService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)

public class CarRepositeryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CarRepositery carRepositery;

    //////////////////
    @TestConfiguration
    static class ParameterServiceImpTestContextConfiguration {
        @Bean
        public ParameterService parameterService(){
            return new ParameterService();
        }
    }
    @Autowired
    private ParameterService service;
    @MockBean
    private ParameterOfCarRepositery repositery;

    @Before
    public void setUp ( ){
        List<Parameter> parameterList = Stream.of(new Parameter("numSetts","4"),new Parameter("ProfitRatio","0.2") )
                .collect(Collectors.toList());
        Mockito.when(repositery.findAll())
                .thenReturn(parameterList);
    }
    @Test
    @Rollback(false)
    @Order(1)
    public void testAddCar1(){
        Car c =new Car(0,null,"car101",null,true,100.0,120.0,"majd");
        ////////////
        List<Parameter> foundList =service.getAllParameters();
        System.out.println("1111111111 "+foundList.get(0).getKeyOfPara());
        c.setNumSets(Integer.parseInt(foundList.get(0).getValueOfPara()));
        System.out.println("222222222 "+foundList.get(0).getValueOfPara());
        double ProfitRatio = Double.parseDouble(foundList.get(1).getValueOfPara());
        double priceOfSale= c.getPriceOfCar()*ProfitRatio;
        priceOfSale=c.getPriceOfCar()+priceOfSale;
        c.setPriceOfSale(priceOfSale);
        System.out.println("********* "+ c.getNumSets());
        ////////////
        entityManager.persist(c);
        int id= c.getCar_id();
        System.out.println("********************** "+id);

        Optional<Car> car = carRepositery.findById(id);
        assertThat(car.get().getCar_id()).isEqualTo(id);
    }


    @Test
    @Rollback(false)
    @Order(2)
    public void testUpdateCar(){
        Car car=carRepositery.GetByname_car("car101");
        car.setPriceOfCar(2000.0);
        carRepositery.save(car);
        Car updateCar= carRepositery.GetByname_car("car101");
        assertThat(updateCar.getPriceOfCar()).isEqualTo(2000.0);
    }

    @Test
    @Rollback(false)
    @Order(3)
    public void testDeleteCar(){
        Car car=carRepositery.GetByname_car("car101");
        carRepositery.deleteById(car.getCar_id());
        Car deleteCar= carRepositery.GetByname_car("car101");
        assertThat(deleteCar).isNull();
    }



}



