package com.example.javaBrainsSpring;

import com.example.javaBrainsSpring.model.Car;
import com.example.javaBrainsSpring.model.Parameter;
import com.example.javaBrainsSpring.repository.CarRepositery;
import com.example.javaBrainsSpring.repository.ParameterOfCarRepositery;
import com.example.javaBrainsSpring.service.CarService;
import com.example.javaBrainsSpring.service.ParameterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK,classes = Application.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.properties")
public class CarControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ParameterOfCarRepositery repositery;
    @Autowired
    private ParameterService service;
    @MockBean
    private CarService carService;
    @Autowired
    private CarRepositery carRepositery;
    @Before
    public void setUp ( ){
        List<Parameter> parameterList = Stream.of(new Parameter("numSetts","4"),new Parameter("ProfitRatio","0.2") )
                .collect(Collectors.toList());
        when(repositery.findAll())
                .thenReturn(parameterList);
    }

    @Test
    public void findallCarTest()throws Exception{
        Car c1 =new Car(0,null,"car22",null,true,100.0,120.0,"majd");
        Car c2 =new Car(0,null,"car2",null,true,100.0,120.0,"majd");
        List<Car> carList= Arrays.asList(c1,c2);

        given(carService.getAllCars()).willReturn(carList);
        mockMvc.perform(
                get("/cars/index")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
    public static String asJsonString(final Object obj){
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @Test
    public void addCarTest()throws Exception{
        Car car1 =new Car(0,null,"car_majd",null,true,100.0,120.0,"majd");
     //////////////////////////////
        List<Parameter> foundList =service.getAllParameters();
        if(car1.getNumSets()==null){
            System.out.println("1111111111 "+foundList.get(0).getKeyOfPara());
            car1.setNumSets(Integer.parseInt(foundList.get(0).getValueOfPara()));
        }
        System.out.println("222222222 "+foundList.get(0).getValueOfPara());
        double ProfitRatio = Double.parseDouble(foundList.get(1).getValueOfPara());
        double priceOfSale= car1.getPriceOfCar()*ProfitRatio;
        priceOfSale=car1.getPriceOfCar()+priceOfSale;
        car1.setPriceOfSale(priceOfSale);
        System.out.println("********* "+ car1.getNumSets());
        ///////////////////////////
        given(carService.addCar(Mockito.any(Car.class))).willReturn(car1);
        ObjectMapper mapper= new ObjectMapper();
        mockMvc.perform(post("/cars/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.valueOf(carRepositery.save(car1)))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(model().attribute("car",instanceOf(Car.class)))
                .andExpect(view().name("index"))
                .andDo(print());
    }
    @Test
    public void updateTest()throws Exception{
        when(carService.getCar(210)).thenReturn(Optional.of(new Car(210,null, "car100", null, true, 100.0, 120.0, "majd")));

        mockMvc.perform(post("/cars/update/{id}",210)
                .accept(MediaType.APPLICATION_JSON)
                .content(String.valueOf(carRepositery.save(carService.getCar(210).get())))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(model().attribute("car",instanceOf(Car.class)))
                .andExpect(view().name("index"))
                .andExpect(status().isOk());
    }
    @Test
    public void deletTest()throws Exception{
        carRepositery.deleteById(208);
        when(carService.getCar(208)).thenReturn(Optional.of(new Car(208,null, "car100", null, true, 100.0, 120.0, "majd")));
         doNothing().when(carService).deleteCar(208);
            mockMvc.perform(get("/cars/delete/{id}",208))
                    .andExpect(status().isOk())
                    .andExpect(view().name("index"));
        verify(carService,times(1)).deleteCar(208);


    }

}
