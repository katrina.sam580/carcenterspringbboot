package com.example.javaBrainsSpring.Controller;


import com.example.javaBrainsSpring.model.Parameter;

import com.example.javaBrainsSpring.service.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ParameterController {

    @Autowired
    private ParameterService parameterService;

    @GetMapping({"/parameter/index"})
    public String showIndex(Model model)
    {
        model.addAttribute("parameters",parameterService.getAllParameters());
        return "index_parameter";
    }


    @GetMapping("/parameter/delete/{parameter_id}")
    public String deleteCar(@PathVariable("parameter_id") Integer parameter_id, Model model) {
        Parameter parameter=  parameterService.getParameter(parameter_id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid parameter_id Id:" + parameter_id));
        parameterService.deleteParameter(parameter);
        model.addAttribute("parameters", parameterService.getAllParameters());

        return "index_parameter";
    }

    @GetMapping("get_parameter/{parameter_id}")
    public Parameter getparameter(@PathVariable("parameter_id") Integer parameter_id) {
        Parameter parameter = parameterService.getParameter(parameter_id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid parameter Id:" + parameter_id));
        return parameter;
    }

}
