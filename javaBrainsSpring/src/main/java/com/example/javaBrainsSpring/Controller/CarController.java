package com.example.javaBrainsSpring.Controller;

import com.example.javaBrainsSpring.model.Car;
import com.example.javaBrainsSpring.repository.CarRepositery;
import com.example.javaBrainsSpring.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/cars")
public class CarController {
    @Autowired
    private CarService carService;

    @Autowired
    private CarRepositery carRepositery;
    @PostMapping(value = "/add")
    public String addCar(Car car , BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-car";
        }
        System.out.println("add car ");
        carService.addCar(car);
        model.addAttribute("cars", carService.getAllCars());
        model.addAttribute("unsoldcars",carService.getAllUnSoldCars());
        return "index";

    }

    @GetMapping({"/index"})
    public String showIndex(Model model)
    {

        model.addAttribute("cars",carService.getAllCars());
        model.addAttribute("unsoldcars",carService.getAllUnSoldCars());
        return "index";
    }
    @GetMapping("list")
    public List<Car> showListCar(Model model) {
        model.addAttribute("cars",  carService.getAllCars());
        model.addAttribute("unsoldcars",carService.getAllUnSoldCars());
        return  carService.getAllCars();
    }
    @RequestMapping("/search")
    public String  viewSearch(Model model,@Param("keyword") String keyword){
        List<Car> listCar = carService.SearchCar(keyword);
        model.addAttribute("cars",listCar);
        model.addAttribute("unsoldcars",carService.getAllUnSoldCars());
        model.addAttribute("keyword",keyword);
        return "index";
    }

    @GetMapping({"/signup"})
    public String showSignUpForm(Car car ) {
        return "add-car";
    }





    @GetMapping ("/edit/{car_id}")
    public String showUpdateForm(@PathVariable("car_id") Integer car_id, Model model) {

        Car car = carService.getCar(car_id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid car Id:" + car_id));
        model.addAttribute("car", car);
        return "update-car";
    }

    @PostMapping ("/update/{car_id}")
    public String updateCar(@PathVariable("car_id") Integer car_id,Car car, BindingResult result, Model model) {
        if (result.hasErrors()) {
            car.setId(car_id);
            return "update-car";
        }
        carService.updateCare(car_id,car);
        model.addAttribute("cars", carService.getAllCars());
        model.addAttribute("unsoldcars",carService.getAllUnSoldCars());
         return "index";
    }

    @GetMapping ("/sale/{car_id}")
    public String showSaleeForm(@PathVariable("car_id") Integer car_id, Model model) {

        Car car = carService.getCar(car_id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid car Id:" + car_id));
        model.addAttribute("car", car);
        System.out.println("controler   "+car.getVersion());
        return "sale-car";
    }

    @PostMapping ("/selling/{car_id}")
    public String SaleCar(@PathVariable("car_id") Integer car_id,Car car, BindingResult result, Model model) {
        if (result.hasErrors()) {
            car.setId(car_id);
            return "sale-car";
        }
        carService.saleCare(car_id,car);
        model.addAttribute("cars", carService.getAllCars());
        model.addAttribute("unsoldcars",carService.getAllUnSoldCars());
        return "redirect:/cars/index";
    }

    @GetMapping("/delete/{car_id}")
    public String deleteCar(@PathVariable("car_id") Integer car_id, Model model) {
      Car car=  carService.getCar( car_id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid car Id:" + car_id));
        carService.deleteCar(car_id);
        model.addAttribute("cars", carService.getAllCars());
        model.addAttribute("unsoldcars",carService.getAllUnSoldCars());
        System.out.println("2222222222222222222 "+car_id);
       // carRepositery.deleteById(car_id);
        return "index";
    }

    //////////////////////////////////
    ////////////////////////////////

    @GetMapping("get_car/{car_id}")
    public Car getCar(@PathVariable("car_id") Integer car_id) {
        Car car = carService.getCar(car_id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + car_id));
        return car;
    }

    @GetMapping("/get")
    public String getParameter()
    {
        return "parameterOfCarRepositery.findByKeyOfPara(";
    }
}

