package com.example.javaBrainsSpring.Controller;

import javax.servlet.http.HttpServletRequest;


import com.example.javaBrainsSpring.config.MessageQueue;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


import com.example.javaBrainsSpring.config.RabbitMQConfiguration;
// @RestController
@Controller
public class MessagePublisher {

    @Autowired
    private RabbitTemplate template;

    @ModelAttribute("message")
    public MessageQueue MessageingQueue() {
        return new MessageQueue();
    }

    @GetMapping("/send/message")
    public String ShowMessageView()
    {
      return "send-message";
    }

    @PostMapping("/sending")
    public String sendMessage(HttpServletRequest request) {

        MessageQueue message = new MessageQueue();
        message.SetEmail(request.getAttribute("email").toString());
        message.SetContent(request.getAttribute("content").toString());
        message.SetDateOfCars(request.getAttribute("dateOfCars").toString());
        template.convertAndSend(RabbitMQConfiguration.TOPIC_EXCHANGE_NAME, RabbitMQConfiguration.ROUTING_KEY, message);

        return "index";
    }



}
