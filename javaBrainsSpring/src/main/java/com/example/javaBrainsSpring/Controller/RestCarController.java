package com.example.javaBrainsSpring.Controller;

import com.example.javaBrainsSpring.model.Car;
import com.example.javaBrainsSpring.repository.CarRepositery;
import com.example.javaBrainsSpring.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestCarController {

    @Autowired
    private CarService carService;

    @Autowired
    private CarRepositery carRepositery;

    @PostMapping(value = "/api/cars/add")
    public void addCar(@RequestBody  Car car) {

        carService.addCar(car);

    }

    @PostMapping ("/api/cars/selling/{car_id}")
    public void SaleCar(@PathVariable Integer car_id, @RequestBody  Car car) {

        carService.saleCare(car_id,car);
    }
}
