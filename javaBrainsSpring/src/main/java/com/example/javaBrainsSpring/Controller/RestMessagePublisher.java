package com.example.javaBrainsSpring.Controller;

import com.example.javaBrainsSpring.config.MessageQueue;
import com.example.javaBrainsSpring.config.RabbitMQConfiguration;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestMessagePublisher {

    @Autowired
    private RabbitTemplate template;

    @PostMapping("/message")
    public String Message_Queue(@RequestBody MessageQueue message)
    {
       template.convertAndSend(RabbitMQConfiguration.TOPIC_EXCHANGE_NAME, RabbitMQConfiguration.ROUTING_KEY, message);

       return "successfull send message to queue";
    }
}
