package com.example.javaBrainsSpring.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.example.javaBrainsSpring.model.User;
import com.example.javaBrainsSpring.web.dto.UserRegistrationDto;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);
}