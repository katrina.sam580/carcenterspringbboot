package com.example.javaBrainsSpring.service;

import com.example.javaBrainsSpring.model.LogUser;
import com.example.javaBrainsSpring.repository.LogRepositery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogService {
    

    @Autowired
    LogRepositery logRepositery;

    public void addUserLog(LogUser logUser)
    {
       logRepositery.save(logUser);
    }
    
}
