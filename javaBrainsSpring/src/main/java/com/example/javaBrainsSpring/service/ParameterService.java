package com.example.javaBrainsSpring.service;


import com.example.javaBrainsSpring.model.Parameter;

import com.example.javaBrainsSpring.repository.ParameterOfCarRepositery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Service
public class ParameterService {

    @Autowired
    private ParameterOfCarRepositery parameterOfCarRepositery;

    public List<Parameter> getAllParameters(){
        List<Parameter> parameters = new ArrayList<>();
        System.out.println( parameterOfCarRepositery.findAll());
        parameterOfCarRepositery.findAll().forEach(parameters::add);
        return parameters;

    }
    public Optional<Parameter> getParameter(Integer parameter_id){
      return  parameterOfCarRepositery.findById(parameter_id);

    }

    @CacheEvict(value = "parametercar",allEntries = true)
    public void deleteParameter(Parameter parameter){
        try {
            Thread.sleep(1000*5);
            System.out.println("thread******************** "+parameter.getParameter_id());
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
        parameterOfCarRepositery.save(parameter);
        return ;
    }



    @Cacheable("parametercar")
    public Parameter getParameteDB(String id) {
        try {
            Thread.sleep(1000*5);
            System.out.println("thread******************** "+id);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }

        Optional<Parameter> p = parameterOfCarRepositery.findBykeyOfPara(id);
        return p.get();

    }
}
