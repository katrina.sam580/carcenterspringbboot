package com.example.javaBrainsSpring.service;

import com.example.javaBrainsSpring.model.Car;
import com.example.javaBrainsSpring.model.Parameter;
import com.example.javaBrainsSpring.repository.CarRepositery;
import com.example.javaBrainsSpring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CarService {

    @Autowired
    private CarRepositery carRepositery;
    @Autowired
    private UserRepository userRepositery;

    @Autowired
    private ParameterService parameterService ;

  
    @PersistenceContext
    private EntityManager entityManager ;

    public List<Car> getAllCars(){
       List<Car> cars = new ArrayList<>();
       carRepositery.findAll().forEach(cars::add);
       return cars;

    }
    public List<Car> getAllUnSoldCars(){
        List<Car> cars = new ArrayList<>();
        List<Car> unsoldcars = new ArrayList<>();
        carRepositery.findAll().forEach(cars::add);
        for(int i=0;i< cars.size();i++){
            if(cars.get(i).getisSale()==false){
                unsoldcars.add(cars.get(i));
            }
        }
        return unsoldcars;

    }
    @Transactional
    public Optional<Car> getCar(Integer car_id){
       return carRepositery.findById(car_id);
    }

    public Car addCar(Car car){
    if(car.getNumSets()==null)
    {
        Parameter parameter = parameterService.getParameteDB("numSetts");
        car.setNumSets(Integer.parseInt(parameter.getValueOfPara()));

    }
        Parameter parameter =  parameterService.getParameteDB("ProfitRatio");

    double ProfitRatio = Double.parseDouble(parameter.getValueOfPara());
    double priceOfSale= car.getPriceOfCar()*ProfitRatio;
    priceOfSale=car.getPriceOfCar()+priceOfSale;
    car.setPriceOfSale(priceOfSale);
    car.setVersion(0);
    System.out.println("version add :" +car.getVersion());
    return carRepositery.save(car);


}
    public Car updateCare(Integer car_id , Car car){
        Optional<Car> car1 = carRepositery.findById(car_id);
        Car car2 = car1.get();
        car2.setNumSets(car.getNumSets());
        car2.setDateSale(car.getDateSale());
        car2.setPriceOfCar(car.getPriceOfCar());
        car2.setNameOfBuyer(car.getNameOfBuyer());
        car2.setNamecar(car.getNamecar());
        System.out.println(car2.getVersion()+ "--------------------------------- "+car2.getVersion()+1);
        car2.setVersion(car2.getVersion()+1);

        Parameter parameter = parameterService.getParameteDB("ProfitRatio");

        double ProfitRatio = Double.parseDouble(parameter.getValueOfPara());
        double priceOfSale= car.getPriceOfCar()*ProfitRatio;
        priceOfSale=car.getPriceOfCar()+priceOfSale;
        car2.setPriceOfSale(priceOfSale);
        return carRepositery.save(car2);
    }
    public void deleteCar(Integer car_id){
       Car c = carRepositery.findById(car_id).get();
       System.out.println("44444444444444444444444444444444444444");
        carRepositery.deleteById(car_id);
        return ;
    }

   @Transactional
    public void saleCare(Integer car_id , Car car){

       Optional<Car> car1 = getCar(car_id);
        Car car2 = car1.get();
        System.out.println("version1******** "+car2.getVersion());
       System.out.println("getNamecar******** "+car2.getNamecar());

    int count = carRepositery.updateCarWithVersion(car_id,car.getNameOfBuyer(),car.getDateSale(),car2.getVersion());
    if(count ==0)
    {
        Optional<Car> car11 = getCar(car_id);
        Car car12 = car11.get();
        System.out.println("version1******** "+car2.getVersion()+"version2 ********"+car12.getVersion());
        throw new RuntimeException("Server is busy , update data failed");
    }
    else {
        Optional<Car> car11 = getCar(car_id);
        Car car12 = car11.get();
        System.out.println("version1******** "+car2.getVersion()+"version2 **after***5555***"+car12.getVersion());
    }
    }

    public List<Car> SearchCar(String keyword){
        if(keyword!= null){
            return carRepositery.search(keyword);

        }
        List<Car> cars = new ArrayList<>();
        carRepositery.findAll().forEach(cars::add);
        return cars;
    }
}
