package com.example.javaBrainsSpring.AspectLog;



import java.util.Collection;


import com.example.javaBrainsSpring.jwtToken.JwtRequestFilter;
import com.example.javaBrainsSpring.model.Role;
import com.example.javaBrainsSpring.model.User;
import com.example.javaBrainsSpring.service.LogService;
import com.example.javaBrainsSpring.service.UserServiceImpl;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.javaBrainsSpring.model.LogUser;


import org.springframework.stereotype.Component;



@Aspect
@Component
public class LoggingAspect {

    Logger log = LoggerFactory.getLogger(LoggingAspect.class);

    @Autowired
    private UserServiceImpl userserviceimpl;

    @Autowired
    private JwtRequestFilter jwt;

    @Autowired
    private LogService logService;

    Logger logging = LoggerFactory.getLogger(LoggingAspect.class);

    @Pointcut(value = "execution(* com.example.javaBrainsSpring.service.CarService.addCar(..) )")
    public void myPointCut()
    {

    }

    @Pointcut(value = "execution(* com.example.javaBrainsSpring.service.CarService.updateCare(..) )")
    public void myPointCutUpdateCar()
    {
       
    }

    @Pointcut(value = "execution(* com.example.javaBrainsSpring.service.CarService.deleteCar(..) )")
    public void myPointCutDeleteCar()
    {
       
    }
    
    @Pointcut(value = "execution(* com.example.javaBrainsSpring.service.CarService.saleCare(..) )")
    public void myPointCutSellingCar()
    {
       
    }

    @Around("myPointCut() || myPointCutUpdateCar() || myPointCutDeleteCar() || myPointCutSellingCar()")
    public Object applicationLogger(ProceedingJoinPoint joinPoint) throws Throwable
    {

      String methodName = joinPoint.getSignature().getName();
      String username = jwt.getUserName();

      logging.info("action : "+methodName + " username : "+username);
      //logService.addUserLog(new LogUser(methodName,username));


      Object obj = joinPoint.proceed();

      return obj;

    }


  
}

