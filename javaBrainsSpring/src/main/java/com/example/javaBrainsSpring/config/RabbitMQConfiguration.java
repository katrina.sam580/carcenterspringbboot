package com.example.javaBrainsSpring.config;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQConfiguration
{

	public static final String TOPIC_EXCHANGE_NAME = "message_queue_exchange";

	public static final String QUEUE_NAME = "message_queue";

    public static final String ROUTING_KEY = "message_routing_key";


	@Bean
	Queue queue()
	{
		return new Queue(QUEUE_NAME, false);
	}

	@Bean
	TopicExchange exchange()
	{
		return new TopicExchange(TOPIC_EXCHANGE_NAME);
	}

	@Bean
	Binding binding(Queue queue, TopicExchange exchange)
	{
		return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY);
	}

    @Bean
    public MessageConverter converter()
    {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate template(ConnectionFactory connectionFactory)
    {
      final RabbitTemplate rabbittemplate = new RabbitTemplate(connectionFactory);
      rabbittemplate.setMessageConverter(converter());
      return rabbittemplate;
    }

}