package com.example.javaBrainsSpring.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
public class Parameter {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer parameter_id;

    @Column(name = "keyOfPara")
    @JsonProperty
     String keyOfPara;

    @Column(name = "valueOfPara")
    @JsonProperty
     String valueOfPara;
    public Parameter(){}
     public Parameter(int parameter_id, String numSetts, int i){ }
    public Parameter(String keyOfPara, String valueOfPara)
    {
        this.keyOfPara=keyOfPara;
        this.valueOfPara=valueOfPara;
    }
    public Parameter(long parameter_id, String keyOfPara, String valueOfPara){

    }

    public void setParameter_id(Integer parameter_id) {
        this.parameter_id = parameter_id;
    }


    public void setKeyOfPara(String keyOfPara) {
        this.keyOfPara = keyOfPara;
    }

    public void setValueOfPara(String valueOfPara) {
        this.valueOfPara = valueOfPara;
    }

    public Integer getParameter_id() {
        return parameter_id;
    }

    public String getKeyOfPara(String numSetts) {
        return keyOfPara;
    }

    public String getKeyOfPara() {
        return keyOfPara;
    }

    public String getValueOfPara() {
        return valueOfPara;
    }
}
