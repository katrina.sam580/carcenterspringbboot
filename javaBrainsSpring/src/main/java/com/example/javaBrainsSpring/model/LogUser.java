package com.example.javaBrainsSpring.model;
import javax.persistence.*;


@Entity
public class LogUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idlog;

    private String actionname;

    private String username;
    
    public LogUser(){}

    public LogUser(String actionname,String username)
    {
        this.username=username;
        this.actionname=actionname;
    }

    public void SetUsername(String username)
    {
        this.username=username;
    }
    public void SetActionname(String actionname)
    {
        this.actionname=actionname;
    }
    public String getUsername()
    {
        return this.username;
    }
    public String getActionName()
    {
        return this.actionname;
    }
}
