package com.example.javaBrainsSpring.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import java.sql.Date;

@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer car_id;

    @Column(name = "version")
    @JsonProperty
//    @Version
    private Integer version;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "num_sets")
    @JsonProperty
    private Integer num_sets;

    @Column(name = "date_sale")
    @JsonProperty
    private Date date_sale;

    @Column(name = "name_car")
     @JsonProperty
    private String name_car;

    @Column(name = "is_sale")
    @JsonProperty
    private boolean is_sale ;

    @Column(name = "price_car")
    @JsonProperty
    private Double price_car;

    @Column(name = "price_sale")
    @JsonProperty
    private Double price_sale;

    @Column(name = "name_buyer")
    @JsonProperty
    private String name_buyer;



   public Car(){}


   public Car(Integer version,Integer numSets,String namecar,Date dateSale,boolean isSale,Double PriceOfCar,Double priceOfSale,String nameOfBuyer)
   {
//        this.car_id=car_id;
       this.version=version;
       this.num_sets=numSets;
       this.date_sale=dateSale;
       this.price_car=PriceOfCar;
       this.price_sale=priceOfSale;
       this.name_buyer=nameOfBuyer;
       this.is_sale=isSale;
       this.name_car=namecar;
   }



    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getVersion() {
        return version;
    }

    

    public boolean isSale() {
        return is_sale;
    }

    public void setId(long id) {
        this.car_id = Math.toIntExact(id);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setCar_id(Integer car_id) {
        this.car_id = car_id;
    }

    public void setDateSale(Date dateSale) {
        this.date_sale = dateSale;
    }

    public void setNameOfBuyer(String nameOfBuyer) {
        this.name_buyer = nameOfBuyer;
    }

    public void setNumSets(Integer numSets) {
        this.num_sets = numSets;
    }

    public void setPriceOfCar(Double priceOfCar) {
        this.price_car = priceOfCar;
    }

    public void setPriceOfSale(Double priceOfSale) {
        this.price_sale = priceOfSale;
    }

    public void setSale(boolean sale) {
        this.is_sale = sale;
    }

    public Integer getNumSets() {
        return this.num_sets;
    }

    public Date getDateSale() {
        return this.date_sale;
    }
    public boolean getisSale() {
        return this.is_sale;
    }
    public Double getPriceOfCar() {
        return this.price_car;
    }

    public Double getPriceOfSale() {
        return this.price_sale;
    }

    public Integer getCar_id() {
        return this.car_id;
    }

    public String getNameOfBuyer() {
        return this.name_buyer;
    }

    public User getUser() {
        return user;
    }

    public void setNamecar(String namecar) {
        this.name_car = namecar;
    }

    public String getNamecar() {
        return this.name_car;
    }
}

