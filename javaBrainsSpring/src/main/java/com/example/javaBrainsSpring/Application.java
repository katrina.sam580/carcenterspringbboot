package com.example.javaBrainsSpring;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import javax.sql.DataSource;


@SpringBootApplication
@EnableCaching
public class Application  implements CommandLineRunner {

	@Autowired
	DataSource dataSource;
    
	public static void main(String[] args) throws InterruptedException
	{
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("DataSource = " + dataSource);
	}

}

