package com.example.javaBrainsSpring.repository;

import com.example.javaBrainsSpring.model.Car;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.sql.Date;
import java.util.List;
import java.util.Optional;


@Repository
public interface CarRepositery extends CrudRepository<Car,Integer> {

    @Modifying
    @Query(value = "update car set name_buyer = :nameOfBuyer, date_sale = :dateSale, version = version + 1, is_sale = true where car_id = :car_id and version = :version",nativeQuery = true)
    int updateCarWithVersion(Integer car_id, String nameOfBuyer, Date dateSale, Integer version);
    ///////////////



@Query("SELECT c FROM Car c WHERE c.name_buyer LIKE %?1%"
        + " OR c.name_car LIKE %?1%"
        + " OR CONCAT (c.car_id,'') LIKE %?1%"
        + " OR CONCAT (c.num_sets,'') LIKE %?1%"
        + " OR CONCAT (c.date_sale,'') LIKE %?1%"
        + " OR CONCAT (c.price_car,'') LIKE %?1%"
        + " OR CONCAT (c.price_sale,'') LIKE %?1%"
        + " OR c.is_sale LIKE %?1%"
)
public List<Car> search(String keyword);

    @Query("select c from Car c where c.name_car = :name_car")
    public Car GetByname_car(String name_car);

}




