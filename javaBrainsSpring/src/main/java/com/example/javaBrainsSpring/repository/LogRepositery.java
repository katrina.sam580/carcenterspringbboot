package com.example.javaBrainsSpring.repository;

import org.springframework.stereotype.Repository;

import com.example.javaBrainsSpring.model.LogUser;

import org.springframework.data.repository.CrudRepository;

@Repository
public interface LogRepositery extends CrudRepository<LogUser,Integer> {
    
}
