package com.example.javaBrainsSpring.repository;

import com.example.javaBrainsSpring.model.Parameter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ParameterOfCarRepositery extends CrudRepository<Parameter,Integer> {

    @Query("select p from Parameter p where p.keyOfPara = :keyOfPara")
    public Optional<Parameter> findBykeyOfPara(String keyOfPara);

    @Query("select p from Parameter p where p.parameter_id = :parameter_id")
    public Optional<Parameter> findById(long parameter_id);

}